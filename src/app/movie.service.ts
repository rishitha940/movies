import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  movieUrl:string="https://api.themoviedb.org/3";

  constructor(private http:HttpClient) { }

  getMoviesList(key:any):Observable<any>{
    let url =  this.movieUrl + '/movie/popular?api_key='+key;
    console.log(url);
    return this.http.get<any>(url); 
  } 
  getMovieDetails(id:any,key:any):Observable<any>{
    let url = this.movieUrl + `/movie/${id}?&api_key=`+key;
    console.log(url);
    let params = new HttpParams();
    params = params.append('movie_id', id);
    return this.http.get<any>(url,{params});
  }
  searchMovies(key:any,search:any):Observable<any>{
    let url =  this.movieUrl + '/search/movie?api_key='+key +'&query='+search;
    console.log(url);
    return this.http.get<any>(url); 
  }
 }
