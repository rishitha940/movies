import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies/movies.component';
import { ViewMovieComponent } from './view-movie/view-movie.component';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [
    MoviesComponent, 
    ViewMovieComponent
  ],
  imports: [
    CommonModule,
    MoviesRoutingModule,
    MatIconModule,MatCardModule,
    FormsModule,MatFormFieldModule,
    MatIconModule,MatInputModule,
    ReactiveFormsModule,
    MatPaginatorModule
  ]
})
export class MoviesModule { }
