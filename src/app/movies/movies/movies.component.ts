import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movieList:any=[]
  key:string="778a537d7f2b582f4f7646964aabc90d";

  constructor(private service:MovieService) { }

  ngOnInit() {
    this.getMovies(this.key);
  }
  getMovies(key){
    this.service.getMoviesList(key).subscribe({
      next:(res)=>{
        console.log('data',res);
        this.movieList=res.results; 
        console.log("list",this.movieList)       
      },error:(err)=>{
        console.log(err,err.error)
      }
    })
  }
  searchMovies(key,search)
  {
    if(search!=''){
      this.service.searchMovies(key,search).subscribe({
        next:(res) => {
        this.movieList=res.results;
       },
      error: (err)=>{
        console.log(err,err.error)
      }
    });
    }
  }
  
}
