import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from 'src/app/movie.service';

@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.component.html',
  styleUrls: ['./view-movie.component.scss']
})
export class ViewMovieComponent implements OnInit {

  id: any;
  key:string="778a537d7f2b582f4f7646964aabc90d";
  singleMovie:any=[]
  spoken_languages: any;

  constructor(private activeRoute:ActivatedRoute,private service:MovieService) { }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params['id'];
    console.log(" this.id", this.id)
    this.getMovieById(this.id,this.key);
     }
  getMovieById(id,key){
    this.service.getMovieDetails(id,key).subscribe({
      next:(res) => {
        this.singleMovie=res;
      },
    error: (err)=>{
      console.log(err,err.error)
    }
  });
         
}
}
