import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';
import { ViewMovieComponent } from './view-movie/view-movie.component';

const routes: Routes = [
  {
    path:'',
    component:MoviesComponent
  },
  {
    path:'movie/view/:id',
    component:ViewMovieComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
